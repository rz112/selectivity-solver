import numpy as np
import pandas as pd
import itertools as it
import cvxopt
import cvxpy as cp
from scipy.sparse import csr_matrix

class Query:
    def __init__(self, ranges, sel):
        """ranges: dictionary, range for each dimension"""
        self.dimension = len(ranges)
        self.ranges = np.array(list(ranges.values()))  # (d,2)
        self.selectivity = sel

class Relation:
    def __init__(self, name, ranges, N):
        self.name = name
        self.dimension = len(ranges)
        self.ranges = np.array(list(ranges.values()))  # (d,2) array
        self.N = N
        self.queries = []

    def addQueries(self, queries):
        self.queries += queries

    def addQuery(self, query):
        self.queries.append(query)

    def solve(self):
        sel = []
        ranges = []  # (n,d,2)
        for query in self.queries:
            sel.append(query.selectivity)
            ranges.append(query.ranges)

        n = len(self.queries)
        d = self.queries[0].dimension
        ranges = np.array(ranges)
        intervals = np.zeros((0, 2*n-1, 2))   # （d, 2n-1, 2)

        for idx in range(d):
            thres = np.sort(ranges[:, idx].flatten())
            interval = np.array(list(zip(thres, thres[1:])))
            intervals = np.vstack((intervals, interval[None]))

        print("intervals:")
        print(intervals.shape)

        # create (2n-1)^d grids
        grids = np.array(list(it.product(*intervals)))   # ((2n-1)^d, d, 2)
        print("grids:")
        print(grids.shape)

        # use sparse matrices
        # label each rectangle, construct A matrix in LP
        data = []
        row = []
        col = []
        for i, rect in enumerate(ranges):
            for j, grid in enumerate(grids):
                if is_in_rect(grid, rect):
                    data.append(1)
                    row.append(i)
                    col.append(j)

        # all grids together form the entire tables
        data.extend(np.ones(len(grids)+1))
        row.extend(n*np.ones(len(grids)+1))
        col.extend(np.arange(len(grids)+1))
        labeled_rect = csr_matrix((data, (row, col)), shape=(n+1, len(grids)+1)) #A

        sel.append(self.N)
        # maximize entropy distribution
        x = cp.Variable(shape=len(grids)+1)
        obj = cp.Maximize(cp.sum(cp.entr(x)))
        constraints = [labeled_rect @ x == sel]
        prob = cp.Problem(obj, constraints)
        prob.solve(solver=cp.SCS, verbose=True)
        print('\nThe optimal solution is:')
        print(x.value)

def label_rect(rect, grids):
    return [i for i, grid in enumerate(grids) if is_in_rect(grid, rect)]

def is_in_rect(grid, rect):
    for d, interval in enumerate(rect):
        if grid[d, 0] < interval[0] or grid[d, 1] > interval[1]:
            return False
    return True

def test_2d():
    print("start...")
    r = Relation("student", {"age": (18, 100), "score": (0, 100)}, 100)
    r.addQuery(Query({"age": (18, 20), "score": (80, 90)}, 12))
    r.addQuery(Query({"age": (10, 40), "score": (75, 85)}, 36))
    r.solve()

def test_3d():
    print("start...")
    r = Relation("student", {"age": (18, 100), "score": (0, 100), "rating": (20,100)}, 100)
    r.addQuery(Query({"age": (18, 20), "score": (80, 90), "rating": (30,70)}, 12))
    r.addQuery(Query({"age": (10, 40), "score": (75, 85), "rating": (50,90)}, 36))
    r.solve()
# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    test_3d()






